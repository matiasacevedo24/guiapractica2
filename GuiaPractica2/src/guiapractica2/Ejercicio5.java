
package guiapractica2;

import java.util.Scanner;


public class Ejercicio5 {

    
    public static void main(String args[]) {
        /*Escribir un algoritmo que lea un valor n por teclado y muestre los
primeros n términos de la sucesión de Fibonacci.
Tip: la sucesión de Fibonacci comienza con 0, luego 1 y a partir de allí
cada nuevo número es la suma de los dos anteriores.*/ 
        
        Scanner s = new Scanner(System.in);
         System.out.print("Ingrese el número de términos de la sucesión de Fibonacci que desea mostrar: ");
        int n = s.nextInt();

        System.out.println("Los primeros " + n + " términos de la sucesión de Fibonacci son:");

       
        if (n >= 1) {
            System.out.print("0");
        }

       
        if (n >= 2) {
            System.out.print(", 1");
        }

       
        int term1 = 0;
        int term2 = 1;
        for (int i = 3; i <= n; i++) {
            int Term = term1 + term2;
            System.out.print(", " + Term);
            term1 = term2;
            term2 = Term;
        }
    }
}

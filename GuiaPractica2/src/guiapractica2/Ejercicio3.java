
package guiapractica2;
//import java.util.Scanner; 
import java.util.Random;
public class Ejercicio3 {

    
    public static void main(String args[]) {
        /*Escribir un programa que ordene un arreglo de números enteros de
manera ascendente. */
        /*Scanner scanner = new Scanner(System.in);*/
        //definir y cargar arreglo
        int[] array = new int[10];
        Random r = new Random();
        for (int i = 1; i < 10; i++)  {
            array[i] = r.nextInt(50);
        }
        int n = array.length; 
        for (int i = 0; i < n - 1; i++)  {
            for (int l = 0; l < 9; l++){
                if (array[l] > array[l + 1]) {
                    int menor = array[l];
                    array[l] = array[l + 1];
                    array[l + 1]= menor;
                }                                
            }
        }
        for (int i = 0; i < 9; i++){
            System.out.println(array[i]);                      
        }           
    }
        
}

